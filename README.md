# Boilerplate for HTML

### Introduction
This boilerplate config to help front-end developer easy to code and enhance performance.

### Feature
* Simple and easy to use
* ESLint: Base style code AirBnB
* Auto reload js and css
* Auto compile SCSS

### Requirement
* Install [nodejs](https://nodejs.org)
* Install [yarn](https://yarnpkg.com)

### Install
* Clone this project
* Run `yarn install` or `npm install`
* Run `bower install` (options)

### Usage
* Run with webpack: `yarn watch`
* Run with webpack-dev-sever: `yarn start`
* Build production: `yarn build`
